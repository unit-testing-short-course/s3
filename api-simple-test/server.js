const express = require('express');
const app = express();
const PORT = 5000;

app.use(express.json());

require('./routes/userRoutes')(app, {}); 

app.listen(PORT, ()=> {
	console.log(`listening to PORT ${PORT}`);
})