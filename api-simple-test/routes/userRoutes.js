const {user} = require('../data');
module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.status(200).send()
	})

	app.get('/user', (req,res) => {
		return res.send(user);
	})

	app.post('/user', (req, res) => {

		if(!req.body.hasOwnProperty('fullName')){
			return res.status(400).send({
				'Error': 'Bad Request - missing required parameters'
			})
		}
		return res.status(200).send({
			'message' : 'User created'
		})
	})
}