const {assert} = require('chai');
const chai = require('chai');
const chaiHTTP = require('chai-http')
const expect = chai.expect;

chai.use(chaiHTTP); 


describe('API_Test_Suite', () => {
	const domain = 'http://localhost:5000';
	it('GET / endpoint', (done) => {
		chai.request(domain)
		.get('/')
		.end((error, res) => {
			expect(res.status).to.equal(200);
			done()
		})
	})

	it('GET /users endpoint', (done) => {
		chai.request(domain)
		.get('/user')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
			done()
		})
	})

	it('POST /user endpoint', (done)=> {
		chai.request(domain)
		.post('/user')
		.type('json')
		.send({
			'fullName' : 'Jason',
			'age' : 45
		})
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done()
		}) 
	})

	it('POST /user endpoint fullName is missing', (done) => {
		chai.request(domain)
		.post('/user')
		.type('json')
		.send({
			'alias' : 'Jason',
			'age' : 28
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})
})